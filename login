import { StyleSheet, StatusBar,Dimensions, Text, View, SafeAreaView, TextInput, TouchableOpacity } from 'react-native';
import React from 'react';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const Login = ({ navigation }) => {
  return (
    <SafeAreaView>
      <StatusBar
        animated={true}
        backgroundColor="grey"
         />
      <View style={styles.form}>
      <TextInput
        style={styles.input}
        placeholder="E-mail"
      />
      
      <TextInput
        style={styles.input}
        placeholder="Password"
      />
      </View>
      
      <TouchableOpacity
        style={styles.button}
        onPress={() => {navigation.navigate('Home')}}
      >
        <Text style={{color:'blue'}}>Sign-in</Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={styles.button}
        onPress={() => {navigation.navigate('SignUp')}}
      >
        <Text style={{color:'blue'}}>Sign-up</Text>
      </TouchableOpacity>
    </SafeAreaView>
  )
}



const styles = StyleSheet.create({
    
    input: {
        borderColor:'blue',
        width: windowWidth*0.9,
        height: 40,
        borderWidth: 1,
        padding: 10,
        margin: 5,
    },
    button: {
        alignItems: "center",
        backgroundColor: "red",
        padding: 10,
        margin: 10,
        borderRadius: 15,
      },
    form: {
        margin: 16,
        paddingTop: windowHeight*0.6,
        
    }
})

export default Login;
