import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Login from './source/screens/login';
import Home from './source/screens/home';
import SignUp from './source/screens/sign-up';
import Applications from './source/screens/myapplications';
import ServiceOffer from './source/screens/serviceoffer';

const Stack = createNativeStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Login" component={Login} options={{headerShown:false}}/>
        <Stack.Screen name="JobMagnet" component={Home} />
        <Stack.Screen name="Sign-up on JobMagnet" component={SignUp} />
        <Stack.Screen name="Application" component={Applications} />
        <Stack.Screen name="My Service" component={ServiceOffer} />
      </Stack.Navigator>
    
    </NavigationContainer>
  );
}


